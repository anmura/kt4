import java.util.Objects;

/**
 * Quaternions. Basic operations.
 */
public class Quaternion {

    private double a;
    private double i;
    private double j;
    private double k;

    /**
     * Constructor from four double values.
     *
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {
        this.a = a;
        this.i = b;
        this.j = c;
        this.k = d;
    }

    /**
     * Real part of the quaternion.
     *
     * @return real part
     */
    public double getRpart() {
        return this.a;
    }

    /**
     * Imaginary part i of the quaternion.
     *
     * @return imaginary part i
     */
    public double getIpart() {
        return this.i;
    }

    /**
     * Imaginary part j of the quaternion.
     *
     * @return imaginary part j
     */
    public double getJpart() {
        return this.j;
    }

    /**
     * Imaginary part k of the quaternion.
     *
     * @return imaginary part k
     */
    public double getKpart() {
        return this.k;
    }

    /**
     * Conversion of the quaternion to the string.
     *
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {

        String s = String.format("%s+%si+%sj+%sk", a, i, j, k);

        s = s.replace("+0.0i", "");
        s = s.replace("+0.0j", "");
        s = s.replace("+0.0k", "");

        s = s.replace("--", "+");
        s = s.replace("+-", "-");


        return s;
    }

    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @return a quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  a quaternion (defined by the <code>toString</code> method)
     */
    public static Quaternion valueOf(String s1) {


        String s = s1.trim();


        s = s.replace("+-", "-");
        s = s.replace("--", "+");
        s = s.replace("-", "+-");

        char[] test = s.toCharArray();
        StringBuilder ijk = new StringBuilder();
        for (char c : test) {
            String strC = String.valueOf(c);
            if (strC.equals("i") || strC.equals("j") || strC.equals("k")) {
                ijk.append(strC);
            }
        }

        String[] parts = s.split("[ijk+]");

        Double[] partsAsDoubles = new Double[4];


        int i = 0;
        for (String part : parts) {
            if (!part.trim().equals("")) {
                partsAsDoubles[i] = Double.parseDouble(part);
                i++;
            }
        }


        if ((ijk.toString().equals("ijk") && partsAsDoubles[3] == null) || (ijk.toString().equals("ij") && partsAsDoubles[2]== null) || (ijk.toString().equals("i") && partsAsDoubles[1] == null) || !"ijk".contains(ijk)) throw new IllegalArgumentException("Invalid string: " + s1);

        if (!ijk.toString().contains("i") && partsAsDoubles[1] != null || !ijk.toString().contains("j") && partsAsDoubles[2] != null || !ijk.toString().contains("k") && partsAsDoubles[3] != null) throw new IllegalArgumentException("Invalid string: " + s1);

        for (int j = 0; j < 4; j++) {
            if (partsAsDoubles[j] == null) {
                partsAsDoubles[j] = 0.;
            }
        }

        double a = partsAsDoubles[0];
        double b = partsAsDoubles[1];
        double c = partsAsDoubles[2];
        double d = partsAsDoubles[3];

        return new Quaternion(a, b, c, d);

    }

    /**
     * Clone of the quaternion.
     *
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Quaternion(a, i, j, k);
    }

    /**
     * Test whether the quaternion is zero.
     *
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        return doubleCompare(a, 0d) && doubleCompare(i, 0d) && doubleCompare(j, 0d) && doubleCompare(k, 0d);
    }

    /**
     * Conjugate of the quaternion. Expressed by the formula
     * conjugate(a+bi+cj+dk) = a-bi-cj-dk
     *
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {

        return new Quaternion(a, -i, -j, -k);
    }

    /**
     * Opposite of the quaternion. Expressed by the formula
     * opposite(a+bi+cj+dk) = -a-bi-cj-dk
     *
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        /*a = -a;
        i = -i;
        j = -j;
        k = -k;*/
        return new Quaternion(-a, -i, -j, -k);
    }

    /**
     * Sum of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     *
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus(Quaternion q) {
        return new Quaternion(a + q.getRpart(), i + q.getIpart(), j + q.getJpart(), k + q.getKpart());
    }

    /**
     * Product of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     *
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times(Quaternion q) {
        double aPart = a * q.getRpart() - i * q.getIpart() - j * q.getJpart() - k * q.getKpart();
        double iPart = a * q.getIpart() + i * q.getRpart() + j * q.getKpart() - k * q.getJpart();
        double jPart = a * q.getJpart() - i * q.getKpart() + j * q.getRpart() + k * q.getIpart();
        double kPart = a * q.getKpart() + i * q.getJpart() - j * q.getIpart() + k * q.getRpart();
        return new Quaternion(aPart, iPart, jPart, kPart);


    }

    /**
     * Multiplication by a coefficient.
     *
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times(double r) {
        return new Quaternion(a * r, i * r, j * r, k * r);
    }

    /**
     * Inverse of the quaternion. Expressed by the formula
     * 1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     * ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
     *
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        if (isZero()) {
            throw new RuntimeException("Division by 0");
        }

        double aPart = (a / (a * a + i * i + j * j + k * k));
        double iPart = ((-i) / (a * a + i * i + j * j + k * k));
        double jPart = ((-j) / (a * a + i * i + j * j + k * k));
        double kPart = ((-k) / (a * a + i * i + j * j + k * k));
        return new Quaternion(aPart, iPart, jPart, kPart);

    }

    /**
     * Difference of quaternions. Expressed as addition to the opposite.
     *
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus(Quaternion q) {
        double aPart = a - q.getRpart();
        double iPart = i - q.getIpart();
        double jPart = j - q.getJpart();
        double kPart = k - q.getKpart();
        return new Quaternion(aPart, iPart, jPart, kPart);
    }

    /**
     * Right quotient of quaternions. Expressed as multiplication to the inverse.
     *
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight(Quaternion q) {
        if (isZero() || q.isZero()) {
            throw new RuntimeException("Division by 0");
        }
        Quaternion inversedQ = q.inverse();
        return times(inversedQ);
    }

    /**
     * Left quotient of quaternions.
     *
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft(Quaternion q) {
        if (isZero() || q.isZero()) {
            throw new RuntimeException("Division by 0");
        }
        Quaternion inversedQ = q.inverse();
        return inversedQ.times(this);
    }

    //https://gist.github.com/mxrguspxrt/4044808
    public static boolean doubleCompare(double a, double b) {
        if (Math.abs(a - b) < 0.0001) return true;
        return false;
    }


    /**
     * Equality test of quaternions. Difference of equal numbers
     * is (close to) zero.
     *
     * @param q second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */

    public boolean equals(Object q) {
        if (q instanceof Quaternion) {
            if (doubleCompare(this.a, ((Quaternion) q).getRpart()) && doubleCompare(this.i, ((Quaternion) q).getIpart()) &&
                    doubleCompare(this.j, ((Quaternion) q).getJpart()) && doubleCompare(this.k, ((Quaternion) q).getKpart()))
                return true;
        }
        return false;
    }


    /**
     * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     *
     * @param q factor
     * @return dot product of this and q
     */
    public Quaternion dotMult(Quaternion q) {

        Quaternion conjugatedQ = q.conjugate();
        Quaternion conjugatedP = conjugate();

        return this.times(conjugatedQ).plus(q.times(conjugatedP)).times(0.5);

    }

    /**
     * Integer hashCode has to be the same for equal objects.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return toString().hashCode(); //Objects.hash(a, i, j, k);
    }

    /**
     * Norm of the quaternion. Expressed by the formula
     * norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     *
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(a * a + i * i + j * j + k * k);
    }

    /**
     * Main method for testing purposes.
     *
     * @param arg command line parameters
     */
    public static void main(String[] arg) {
        System.out.println(valueOf("-1-2i-4j"));
    }
    /*    Quaternion arv1 = new Quaternion(-1., 1, 2., -2.);
        if (arg.length > 0)
            arv1 = valueOf(arg[0]);
        System.out.println("first: " + arv1.toString());
        System.out.println("real: " + arv1.getRpart());
        System.out.println("imagi: " + arv1.getIpart());
        System.out.println("imagj: " + arv1.getJpart());
        System.out.println("imagk: " + arv1.getKpart());
        System.out.println("isZero: " + arv1.isZero());
        System.out.println("conjugate: " + arv1.conjugate());
        System.out.println("opposite: " + arv1.opposite());
        System.out.println("hashCode: " + arv1.hashCode());
        Quaternion res = null;
        try {
            res = (Quaternion) arv1.clone();
        } catch (CloneNotSupportedException e) {
        }
        System.out.println("clone equals to original: " + res.equals(arv1));
        System.out.println("clone is not the same object: " + (res != arv1));
        System.out.println("hashCode: " + res.hashCode());
        res = valueOf(arv1.toString());
        System.out.println("string conversion equals to original: "
                + res.equals(arv1));
        Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
        if (arg.length > 1)
            arv2 = valueOf(arg[1]);
        System.out.println("second: " + arv2.toString());
        System.out.println("hashCode: " + arv2.hashCode());
        System.out.println("equals: " + arv1.equals(arv2));
        res = arv1.plus(arv2);
        System.out.println("plus: " + res);
        System.out.println("times: " + arv1.times(arv2));
        System.out.println("minus: " + arv1.minus(arv2));
        double mm = arv1.norm();
        System.out.println("norm: " + mm);
        System.out.println("inverse: " + arv1.inverse());
        System.out.println("divideByRight: " + arv1.divideByRight(arv2));
        System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
        System.out.println("dotMult: " + arv1.dotMult(arv2));
    } */
}
// end of file
